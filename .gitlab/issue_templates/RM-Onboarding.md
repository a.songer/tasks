## On-Boarding

Trainee: `your_username`
Release Manager: Release Manager in your timezone. See https://about.gitlab.com/release-managers/ for details

- [ ] Trainee: Assign yourself and the Release Manager to this issue.
- [ ] Trainee: Set up a sync or have an async chat with the Release Manager you are shadowing to go over any initial questions and your learning style
  and align on your expectations for your shadowing experience.

### Usernames

Trainee: Make a note of your `GitLab.com` and `dev.gitlab.org` usernames and add them to this issue.

| Instance           | Username |
|:-------------------|:---------|
| gitlab.com         |          |
| dev.gitlab.org     |          |
| ops.gitlab.net     |          |
| release.gitlab.net |          |

### Access request

- [ ] Trainee: Add your information to the [`config/release_managers.yml`](https://gitlab.com/gitlab-org/release-tools/blob/master/config/release_managers.yml)
  file in release-tools and open a merge request, linking to this issue.
- [ ] Trainee: Open a merge request, linking to this issue, that adds your information to the [`data/release.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/releases.yml) file under `manager_apac_emea` or `manager_americas` for the appropriate version when you plan to shadow the Release Manager, [example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133730).
- [ ] Trainee: make sure you can log in to `ops.gitlab.net`. After log in, please change your username to be the same as it is on gitlab.com
- [ ] Trainee: make sure you can log in to `release.gitlab.net`. After log in, please change your username to be the same as it is on gitlab.com
- [ ] Trainer: Add user as a `Maintainer` in [the delivery group](https://gitlab.com/gitlab-com/gl-infra/delivery/-/project_members)

### Tool setup

Trainee: Ensure you have completed all the steps on `Access Request` before doing this section.

- [ ] Trainee: Make sure you have [release-tools](https://gitlab.com/gitlab-org/release-tools) cloned locally, and [set it up](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#setup)
- [ ] Trainee: Make sure you have chatops access by running `/chatops run auto_deploy status` in `#chat-ops-test` Slack channel. If not, ask somebody who does
  to run the following command in Slack: `/chatops run member add USER
  gitlab-com/chatops --ops`, replacing USER with your username on
  ops.gitlab.net.

### First Tasks

- [ ] Trainee: Join the following Slack Channels:
    - #announcements - Shows deployments moving through the various environments.
    - #f_upcoming_release - Channel used by release managers to perform daily activities.
    - #g_delivery - Delivery group channel. A good place to ask any questions.
    - #incident-management - Any declared incidents will display in this channel. You will find links to the associated incident slack channel and issue.
    - #production - Contains a lot of production discussions, but also a place to see all feature flags being toggled on production.
    - #releases - Contains messages about monthly releases, patch releases, stable branch failures, and merge train.
- [ ] Trainee: Read through the [release guides](https://gitlab.com/gitlab-org/release/docs/blob/master/README.md)
- [ ] Trainee: Read through the [deployment and releases documentation](https://about.gitlab.com/handbook/engineering/deployments-and-releases/)
- [ ] Trainee: Read through the [release documentation](https://about.gitlab.com/handbook/engineering/releases/)
- [ ] Trainee: Read through the [deployment documentation](https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/)
- [ ] Trainee: Read the deploy [docs](https://gitlab.com/gitlab-org/release/docs/tree/master#deployment)

### Dashboards and issues to familiarize with and watch

These are reviewed weekly during the Delivery group weekly meeting.

- [ ] [Auto-deploy packages dashboard](https://dashboards.gitlab.net/d/delivery-auto_deploy_packages/delivery-auto-deploy-packages-information?orgId=1)
- [ ] [Deployment frequency and lead time](https://gitlab.com/gitlab-org/gitlab/-/pipelines/charts?chart=deployment-frequency)
- [ ] Familiarize with the [deployment blockers epic](https://docs.google.com/document/d/1OjEjjqeGFPFea5rN-z83osHgVPmAd9tT6FrPawZwB3o/edit#bookmark=id.yhuzt5ccbdyo)
- [ ] [Release management dashboard](https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1)

### Tasks to shadow

Not every task will happen during your shadowing shift (for example a critical patch release). This is a non-exhaustive list of some major tasks to shadow when they occur.

* [ ] Patch release
* [ ] Rollback practice
* [ ] Promoting a package
* [ ] Patch release
* [ ] Monthly release
* [ ] Deployment blocking incident
* [ ] Pick into autodeploy / manually push an MR through to production
* [ ] Declaring an incident
* [ ] Opening a release/tasks issue for a failure
* [ ] Execute the PDM pipeline
* [ ] Critical patch release

/label ~"onboarding" ~"team::Delivery"
